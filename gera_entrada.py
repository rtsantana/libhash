import random
f = open('input.txt', 'w')
m = int(input("Quantos valores? "))
n = int(input("Quantos conjuntos? "))
r = int(input("Qual limite superior de chaves? "))
f.write("#"+str(m)+":"+str(n)+"\n")
for i in range(m):
    f.write(str(random.randint(0, r))+":"+str(random.randint(0, 2000000000))+"\n")
f.close()
