#include "libhash.h"

int hashTable::hash(int chave)
{
	return chave%tam_hash;
}

int hashTable::get_hash_block(int i)
{
	return i/tam_bloco;
}

void hashTable::expand()
{
	int i, j;
	entrada *novos_dados = new entrada[tam_hash*2];
	pthread_mutex_t *novos_mutex = new pthread_mutex_t[num_blocos*2];
	hashTable new_hash(tam_hash*2, num_blocos, dados, tam_hash);
	tam_hash *= 2;
	new_hash.export_data(tam_hash, novos_dados);
	delete [] dados;
	dados = novos_dados;
	for(i = num_blocos; i < num_blocos*2; i++)
		pthread_mutex_init(&mutex[i], NULL);
	delete [] mutex;
	mutex = novos_mutex;
	tam_bloco *= 2;
}

hashTable::hashTable(int m, int n, entrada *input, int m2)
{
	tam_hash = m;
	num_entradas = 0;
	tam_bloco = m/n;
	num_blocos = n;
	dados = new entrada[tam_hash];
	mutex = new pthread_mutex_t[num_blocos];
	for(int i = 0; i < num_blocos; i++)//Inicializa todos os mutex
		pthread_mutex_init(&mutex[i], NULL);
	new_hash_c = 0;
	new_hash = false;
	pthread_mutex_init(&mutex_new_hash_c, NULL);
	pthread_mutex_init(&mutex_new_hash, NULL);
	if( m2 == -1 )
	{
		m2 = tam_hash;
		temp_hash = false;
	}else
		temp_hash = true;
	for(int i = 0; i < m2; i++)//Inicializa toda a tabela hash
	{
		if( input[i].chave != -1 )
			add(input[i]);
	}
}

hashTable::~hashTable()
{
	int n = 0;
	do
	{
		for(int i = 0; i < num_blocos; i++)
		{//Verifica se todos os mutex est�o livres
			if( !pthread_mutex_trylock(&mutex[i]) )
			{//Verifica se o mutex est� livre
				n++;
			}
		}
	} while( n < num_blocos );
	for(int i = 0; i < num_blocos; i++)
    {//Libera todos os mutex
		pthread_mutex_unlock(&mutex[i]);
		pthread_mutex_destroy(&mutex[i]);
	}
	pthread_mutex_lock(&mutex_new_hash);
	pthread_mutex_unlock(&mutex_new_hash);
	pthread_mutex_destroy(&mutex_new_hash);
	pthread_mutex_lock(&mutex_new_hash_c);
	pthread_mutex_unlock(&mutex_new_hash_c);
	pthread_mutex_destroy(&mutex_new_hash_c);
}

entrada hashTable::get(entrada x)
{
	int hash = this->hash(x.chave);
	int hash_i = hash;
	int block = get_hash_block(hash);
	int block_i = block;
	entrada ret;
	while( ret.chave == -1 && ret.dado == -1 )
	{//Enquanto a tabela n�o for totalmente percorrida ou o valor encontrado
		pthread_mutex_lock(&mutex[block_i]);
		if( this->new_hash )
		{
			pthread_mutex_unlock(&mutex[block_i]);
			pthread_mutex_lock(&mutex_new_hash_c);
			new_hash_c--;
			pthread_mutex_unlock(&mutex_new_hash_c);
			while( pthread_mutex_trylock(&mutex_new_hash) && new_hash_c && this->new_hash );
			return get(x);
		}
		while( hash_i < (block_i+1)*tam_bloco && ret.chave == -1 && ret.dado == -1 )
		{//Percorre o bloco procurando a chave desejada
			if( dados[hash_i].chave == x.chave )
				ret = dados[hash_i];
			else if( dados[hash_i].chave == -1 )
				ret.dado = hash_i;

			hash_i++;
		}
		pthread_mutex_unlock(&mutex[block_i]);
		if( ret.chave == -1 )
		{
			block_i++;
			if( block_i == num_blocos )
			{
				block_i = 0;
				hash_i = 0;
			}
		}
	}
	return ret;
}

bool hashTable::add(entrada x)
{
	entrada element = get(x);
	bool ret = false;
	if( element.chave != x.chave )
	{//A entrada n�o existe
		int block;
		if( element.chave == -1 )
			element.chave = element.dado;
		else
			element.chave = x.chave;
		block = get_hash_block(element.chave);
        pthread_mutex_lock(&mutex[block]);
		if( 0 && num_entradas >= 0.7*tam_hash )
		{//Aumentar o tamanho da hash
			int i, n = 0;
			pthread_mutex_lock(&mutex_new_hash);
			pthread_mutex_unlock(&mutex[block]);
			new_hash_c = 0;
			pthread_mutex_lock(&mutex_new_hash_c);
			for(i = 0; i < num_blocos; i++)
				if( pthread_mutex_trylock(&mutex[i]) )
					new_hash_c++;
				else
					n++;
			pthread_mutex_unlock(&mutex_new_hash_c);
			do
			{
				for(i = 0; i < num_blocos; i++)
					if( !pthread_mutex_trylock(&mutex[i]) )
						n++;
			}while( n < num_blocos );
			this->new_hash = true;
			expand();
			for(i = 0; i < num_blocos/2; i++)
				pthread_mutex_unlock(&mutex[i]);
            while( new_hash_c ) ;
            this->new_hash = false;
			pthread_mutex_unlock(&mutex_new_hash);
			return add(x);
		}
        dados[element.chave] = x;
		pthread_mutex_unlock(&mutex[block]);
		num_entradas++;
		ret = true;
	}
	return ret;
}

bool hashTable::set(entrada x)
{
	entrada element = get(x);
	bool ret = false;
	if( element.chave != x.chave )
		ret = add(x);//Entrada ser� adicionada, pois n�o foi encontrada
	else
	{//Valor apenas ser� alterado
		int hash = this->hash(element.chave);
		int block = get_hash_block(hash);
		pthread_mutex_lock(&mutex[block]);
		if( this->new_hash )
		{
			pthread_mutex_unlock(&mutex[block]);
			pthread_mutex_lock(&mutex_new_hash_c);
			new_hash_c--;
			pthread_mutex_unlock(&mutex_new_hash_c);
			while( pthread_mutex_trylock(&mutex_new_hash) && new_hash_c && this->new_hash );
			return set(x);
		}
		dados[hash].dado = x.dado;
		pthread_mutex_unlock(&mutex[block]);
		ret = true;
	}
	return ret;
}

bool hashTable::remove(entrada x)
{
	entrada element = get(x);
	bool ret = false;
	if( element.chave == x.chave )
	{//Valor ser� removido
		int hash = this->hash(element.chave);
		int block = get_hash_block(hash);
		pthread_mutex_lock(&mutex[block]);
		if( this->new_hash )
		{
			pthread_mutex_unlock(&mutex[block]);
			pthread_mutex_lock(&mutex_new_hash_c);
			new_hash_c--;
			pthread_mutex_unlock(&mutex_new_hash_c);
			while( pthread_mutex_trylock(&mutex_new_hash) && new_hash_c && this->new_hash );
			return remove(x);
		}
		dados[hash].reset();
		pthread_mutex_unlock(&mutex[block]);
		ret = true;
	}
	return ret;
}

void hashTable::print(entrada x)
{
	entrada element = get(x);
	if( element.chave == x.chave )
		cout << element.chave << ":" << element.dado << endl;//printf("%d:%d\n", element.chave, element.dado);
	else
		cout << "null" << endl;
}

void hashTable::printall()
{
	int i, j;
	entrada *element = dados;
	for(i = 0; i < num_blocos; i++)
	{//Percorre os n blocos
		for(j = 0; j < tam_bloco; j++, element++)
		{//De tamanho m/n
			if( element->chave != -1 )
				cout << element->chave << ":" << element->dado << endl;//printf("%d:%d\n", element->chave, element->dado);
			else
				cout << "null" << endl;
		}
	}
}

void hashTable::export_data(int m, entrada *dados)
{
    while( m )
		dados[--m] = this->dados[m];
}
