#include <cstdlib>
#include "libhash.h"

struct arg_thread
{
	int			tID;
	bool		pronto;
};

int m_entradas, n_blocos;
hashTable *hTable;

hashTable *initializeHash()
{
	ifstream input("input.txt");
	string line;
	entrada *dados;
	int separador;
	int i = 0;
	getline(input, line);
	separador = line.find(":");
	m_entradas = atoi(line.substr(1, separador).c_str());
	n_blocos = atoi(line.substr(separador+1).c_str());
	dados = new entrada[m_entradas];
	while( getline(input, line) )
	{
		separador = line.find(":");
        dados[i].chave = atoi(line.substr(0, separador).c_str());
        dados[i++].dado = atoi(line.substr(separador+1).c_str());
	}
	input.close();
	return new hashTable(m_entradas, n_blocos, dados);
}

void *thread( void *arg )
{
	arg_thread	*arg_t = (arg_thread	*)arg;
	entrada		e;
	cout << "Thread - " << arg_t->tID << " : iniciada." << endl;
	e.chave = 43;
	e.dado = 123123123;
	switch( arg_t->tID )
	{
		case 0:
			hTable->get(e);
			e.chave = 44;
			hTable->print(e);
		break;
		case 1:
			hTable->set(e);
		break;
		case 2:
			hTable->print(e);
			e.chave = 44;
			hTable->add(e);
		break;
		case 3:
			hTable->remove(e);
		break;
		default:
		break;
	}
	arg_t->pronto = true;
	cout << "Thread - " << arg_t->tID << " : finalizada." << endl;
}

int main()
{
	arg_thread	arg_threads[4];
	pthread_t	threads[4];
	int			ret_threads[4];
	int 		i;
	hTable = initializeHash();
	hTable->printall();
	for(i = 0; i < 4; i++)
	{
		arg_threads[i].tID = i;
		arg_threads[i].pronto = false;
		ret_threads[i] = pthread_create(&threads[i], NULL, thread, (void *)&arg_threads[i]);
	}
	for(i = 0; i < 4; i++)
		pthread_join(threads[i], 0);
	while(	arg_threads[0].pronto == false ||
			arg_threads[1].pronto == false ||
			arg_threads[2].pronto == false ||
			arg_threads[3].pronto == false )
			;
	hTable->printall();
	delete hTable;
	return 0;
}
