#ifndef __LIBHASH_H__
#define __LIBHASH_H__

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <list>

using namespace std;

struct entrada
{
	int chave;
	int dado;
public:
	entrada(int key = -1, int value = -1)
	{
		chave=key;
		dado=value;
	}
	inline void reset()
	{
		chave = -1;
		dado = -1;
	}
	inline entrada& operator=(const entrada& a)
	{
		this->chave = a.chave;
		this->dado = a.dado;
		return *this;
	}
	friend bool operator==(const entrada& a, const entrada& b)
	{
		if( a.chave == b.chave && a.dado == b.dado )
			return true;
		return false;
	}
	friend bool operator!=(const entrada& a, const entrada& b)
	{
		return !(a == b);
	}
};

class hashTable
{
private:
	int num_entradas;//N�mero de entradas j� informadas
	int tam_hash;//N�mero m�ximo de entradas
	int tam_bloco;//Tamanho dos blocos(n�mero de entradas por bloco)
	int num_blocos;//N�mero de blocos
	bool new_hash;//Signal de que a hash foi aumentada
	bool temp_hash;//Verifica se � uma hash tempor�ria
	int new_hash_c;//Contador de funcoes que foram pausadas por causa do aumento da hash
	pthread_mutex_t *mutex, mutex_new_hash, mutex_new_hash_c;//Lista de mutex para os blocos, mutex para aumentar a hash, mutex contador para recalcular hashs
	entrada *dados;//Dados da hash

	//hash(x), retorna a hash apartir de uma chave
	int hash(int chave);

	//get_hash_block(x), retorna o bloco onde se encontra a hash
	int get_hash_block(int i);

	//expand(), dobra o tamanho da tabela hash atual
	void expand();

public:
	//hashTable(m,n,entrada)o qual inicializa uma tabela hash com
	//m vari�veis do tipo entrada sincronizados em n blocos(m m�ltiplo de n)
	hashTable(int m, int n, entrada *input, int m_2 = -1);

	//del/free hashTable libera a tabela hash ap�s adquirir controle de todos os blocos
	~hashTable();

	//get(x), retorna o elemento de chave x se este existir
	entrada get(entrada x);

	//add(x), adiciona um elemento de chave x se este n�o existir
	bool add(entrada x);

	//set(x), adiciona um elemento de chave x se este n�o existir, ou muda seu valor caso j� exista
	bool set(entrada x);

	//delete(x), remove um elemento de chave x se este existir
	bool remove(entrada x);

	//print(x), imprime o conte�do do elemento de chave x ou null se este n�o existir
	void print(entrada x);

	//printall(x), imprime todos os elementos da tabela
	void printall();

	//Exporta todas as entradas da tabela atual
    void export_data(int m, entrada *dados);
};

#endif // __MAIN_H__
