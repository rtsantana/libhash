all:	libhash	test

libhash:	libhash.o
	g++ -shared obj/libhash.o -lpthread -o libhash.so

libhash.o:
	g++ -c -fPIC src/libhash.cpp -o obj/libhash.o

test:	test.o
	g++ obj/test.o -L. -lhash -lpthread -o test

test.o:
	g++ -c src/test.cpp -o obj/test.o

clean:
	rm -rf obj/*.* *.so test

teste:
	export LD_LIBRARY_PATH=$(CURDIR);\
	./test 1>out.log 2>error.log
